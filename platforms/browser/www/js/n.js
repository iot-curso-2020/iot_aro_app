function inicio_4pantallas(x) {
    clavija = document.getElementById('clavija');
    humedad = document.getElementById('humedad');
    temperatura = document.getElementById('temperatura');
    inicio = document.getElementById('inicio');
    switch(x){
    	case 1:
    	clavija.style.display = '';
    	humedad.style.display = 'none';
    	temperatura.style.display = 'none';
    	inicio.style.display = 'none';
    	break;

    	case 2:
    	clavija.style.display = 'none';
    	humedad.style.display = '';
    	temperatura.style.display = 'none';
    	inicio.style.display = 'none';	
    	break;

    	case 3:
    	clavija.style.display = 'none';
    	humedad.style.display = 'none';
    	temperatura.style.display = '';
    	inicio.style.display = 'none';
    	break;

    	default:
    	clavija.style.display = 'none';
    	humedad.style.display = 'none';
    	temperatura.style.display = 'none';
    	inicio.style.display = '';
    }
}

var t = true;
var h = true;
var r = true;

function inicio_4pantallas_img (selector)
{
    switch(selector) 
    {
    case 1:
        t = true;
        h = true;
        r = false;
    
        fresh();
        break;
    case 2:
        t = true;
        h = false;
        r = true;  
        fresh();
        break;
    case 3:
        t = false;
        h = true;
        r = true;
        fresh();
        break;
    case 0:
        t = true;
        h = true;
        r = true;
        fresh();
        break;

    default:
        fresh();
    }
}

function fresh()
{
    temp();
    hum();
    gleta();
    
}


function temp() {
    if (t) {
        document.getElementById('ico_temperatura').src = 'img/w.svg';
        t = false;
    } 
    else{
        document.getElementById('ico_temperatura').src = 'img/w2.svg';
        t = true;
    }   
}

function hum() {
    if (h) {
        document.getElementById('ico_humedad').src = 'img/u.svg';
        h = false;
    } 
    else{
        document.getElementById('ico_humedad').src = 'img/u2.svg';
        h = true;
    }
}

function gleta() {
    if (r) {
        document.getElementById('ico_clavija').src = 'img/p.svg';
        r = false;
    } 
    else{
        document.getElementById('ico_clavija').src = 'img/p2.svg';
        r = true;
    }
}



function cargar_datos() 
{   
    recibir_datos(1);
    recibir_clavija();
    fresh();

}

// COMO USAR

// Dentro de HEAD ( <script type='text/javascript' src='nav.js'></script>   )

// Dentro de Body ( )

// Dentro de HTML | BOTON   (       )

// Dentro del HTML | CONTENEDOR     (<img id='on' onclick='menu()' src='' alt='Boton'>)

// document.getElementById('').src = '';
// http://lineadecodigo.com/javascript/cambiar-dinamicamente-una-imagen-con-javascript/