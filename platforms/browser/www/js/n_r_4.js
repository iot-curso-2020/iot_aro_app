var salida_n_r_4 = true;
var power_n_r_4 = true;

function n_r_4_img (selector)
{
    switch(selector) 
    {
    case 1:
        salida_n_r_4 = false;
        power_n_r_4 = true; 
        fresh_4();
        break;
    case 0:
        salida_n_r_4 = true;
        power_n_r_4 = false; 
        fresh_4();
        break;
    default:
        fresh_4();
    }
}

function fresh_4()
{
    salida_n_r_4_img();
    power_n_r_4_img();
}

function salida_n_r_4_img() {
    if (salida_n_r_4) {
        document.getElementById('salida_n_r_4').src = 'img/hr4.svg';
        salida_n_r_4 = false;
    } 
    else{
        document.getElementById('salida_n_r_4').src = 'img/h2.svg';
        salida_n_r_4 = true;
    } 
}
function power_n_r_4_img() {
    if (power_n_r_4) {
        document.getElementById('power_n_r_4').src = 'img/o.svg';
        power_n_r_4 = false;
    } 
    else{
        document.getElementById('power_n_r_4').src = 'img/o2.svg';
        power_n_r_4 = true;
    }   
}

function n_r_4_vista(x) {
    n_r_4_salida = document.getElementById('n_r_4_salida');
    n_r_4_apagado = document.getElementById('n_r_4_apagado');

    switch(x){
        
        case 1:
        n_r_4_salida.style.display = '';
        n_r_4_apagado.style.display = 'none';
        modo[4] = 1;
        break;

        case 0:
        n_r_4_salida.style.display = 'none';
        n_r_4_apagado.style.display = '';
        modo[4] = 0;
        break;

        default:
        n_r_4_salida.style.display = 'none';
        n_r_4_apagado.style.display = 'none';
    }
}
