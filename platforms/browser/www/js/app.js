function inicio_4pantallas(x) {
    clavija = document.getElementById('clavija');
    humedad = document.getElementById('humedad');
    temperatura = document.getElementById('temperatura');
    inicio = document.getElementById('inicio');
    switch(x){
    	case 1:
    	clavija.style.display = '';
    	humedad.style.display = 'none';
    	temperatura.style.display = 'none';
    	inicio.style.display = 'none';
    	break;

    	case 2:
    	clavija.style.display = 'none';
    	humedad.style.display = '';
    	temperatura.style.display = 'none';
    	inicio.style.display = 'none';	
    	break;

    	case 3:
    	clavija.style.display = 'none';
    	humedad.style.display = 'none';
    	temperatura.style.display = '';
    	inicio.style.display = 'none';
    	break;

    	default:
    	clavija.style.display = 'none';
    	humedad.style.display = 'none';
    	temperatura.style.display = 'none';
    	inicio.style.display = '';
        setInterval('recibir_datos()',3000);
    }
}

var t = true;
var h = true;
var r = true;

function inicio_4pantallas_img (selector)
{
    switch(selector) 
    {
    case 1:
        t = true;
        h = true;
        r = false;
    
        fresh();
        break;
    case 2:
        t = true;
        h = false;
        r = true;  
        fresh();
        break;
    case 3:
        t = false;
        h = true;
        r = true;
        fresh();
        break;
    case 0:
        t = true;
        h = true;
        r = true;
        fresh();
        break;

    default:
        fresh();
    }
}

function fresh()
{
    temp();
    hum();
    gleta();
}


function temp() {
    if (t) {
        document.getElementById('ico_temperatura').src = 'img/t.svg';
        t = false;
    } 
    else{
        document.getElementById('ico_temperatura').src = 'img/t2.svg';
        t = true;
    }   
}

function hum() {
    if (h) {
        document.getElementById('ico_humedad').src = 'img/h.svg';
        h = false;
    } 
    else{
        document.getElementById('ico_humedad').src = 'img/h2.svg';
        h = true;
    }
}

function gleta() {
    if (r) {
        document.getElementById('ico_clavija').src = 'img/p.svg';
        r = false;
    } 
    else{
        document.getElementById('ico_clavija').src = 'img/p2.svg';
        r = true;
    }
}
strText = '';
salida_4_n_r = '';
var r = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
var modo = [0,0,0,0,0];

function recibir_datos()
{
var request = new XMLHttpRequest();
request.onreadystatechange = function ()
   {
      if (request.readyState == 4) {
         if (request.status == 200) {
            if (request.responseXML != null) {
               document.getElementsByClassName('TEMPERATURA')[0].innerHTML =
                     this.responseXML.getElementsByTagName('TEMPERATURA')[0].childNodes[0].nodeValue;
               document.getElementsByClassName('HUMEDAD')[0].innerHTML =
                     this.responseXML.getElementsByTagName('TEMPERATURA')[0].childNodes[0].nodeValue;                     
               document.getElementsByClassName('HORA')[0].innerHTML =
                     this.responseXML.getElementsByTagName('HORA')[0].childNodes[0].nodeValue;
               document.getElementsByClassName('MINUTO')[0].innerHTML =
                     this.responseXML.getElementsByTagName('MINUTO')[0].childNodes[0].nodeValue; 
              var count;
              var num_an = this.responseXML.getElementsByTagName('TABLA').length;  
              for (count = 0; count < num_an; count++) 
              {              
              r[count] = this.responseXML.getElementsByTagName('TABLA')[count].childNodes[0].nodeValue;
              }
            }
         }
      }
   };
request.open('GET', 'xml/s.xml', true);
request.send(null);
}

function enviar_datos()
{
    nocache = '&nocache=' + Math.random() * 1000000;
    var request = new XMLHttpRequest();
    strText = '&txt=' + document.getElementById(0).value + ',' + document.getElementById(1).value + ',' + document.getElementById(2).value + ',' + document.getElementById(3).value + ',' + document.getElementById(4).value + ',' + document.getElementById(5).value + ',' + document.getElementById(6).value + ',' + document.getElementById(7).value + ',' + document.getElementById(8).value + ',' + document.getElementById(9).value + ',' + document.getElementById(10).value + ',' + document.getElementById(11).value + ',' + document.getElementById(12).value + ',' + document.getElementById(13).value;
    request.open('GET', 'guardar.iom' + strText + ',' + ',' + '&end=end' + nocache, true);
    request.send(null);
}


function myFunction (numero) 
{
  document.getElementsByClassName('TABLA')[numero].innerHTML = document.getElementById(numero).value;
  r[numero] = document.getElementById(numero).value;   
}

function range ()
{
var count = 0;
for (count = 0; count < r.length; count++)
   {
      document.getElementsByClassName('TABLA')[count].innerHTML = r[count];
      document.getElementById(count).value = r[count];
   }
setTimeout('range()', 1000);
}
function n_r(x) {
    r1 = document.getElementById('r1');
    r2 = document.getElementById('r2');
    r3 = document.getElementById('r3');
    r4 = document.getElementById('r4');
    switch(x){
        case 1:
        r1.style.display = '';
        r2.style.display = 'none';
        r3.style.display = 'none';
        r4.style.display = 'none';
        break;

        case 2:
        r1.style.display = 'none';
        r2.style.display = '';
        r3.style.display = 'none';
        r4.style.display = 'none';    
        break;

        case 3:
        r1.style.display = 'none';
        r2.style.display = 'none';
        r3.style.display = '';
        r4.style.display = 'none';
        break;

        case 4:
        r1.style.display = 'none';
        r2.style.display = 'none';
        r3.style.display = 'none';
        r4.style.display = '';
        break;        

        default:
        r1.style.display = '';
        r2.style.display = 'none';
        r3.style.display = 'none';
        r4.style.display = 'none';
    }
}

function cargar_datos()
{
    recibir_datos();

    inicio_4pantallas_img();

    range();

    n_r_1_img(modo[0]);
    n_r_2_img(modo[1]);
    n_r_3_img(modo[2]);
    n_r_4_img(modo[3]);

    n_r_1_vista(modo[0]);
    n_r_2_vista(modo[1]);
    n_r_3_vista(modo[2]);
    n_r_4_vista(modo[3])
}

var sun_n_r_1 = true;
var power_n_r_1 = true;

function n_r_1_img (selector)
{
    switch(selector) 
    {
    case 1:
        sun_n_r_1 = false;
        power_n_r_1 = true; 
        fresh_1();
        break;
    case 0:
        sun_n_r_1 = true;
        power_n_r_1 = false; 
        fresh_1();
        break;
    default:
        fresh_1();
    }
}

function fresh_1()
{
    sun_n_r_1_img();
    power_n_r_1_img();
}

function sun_n_r_1_img() {
    if (sun_n_r_1) {
        document.getElementById('sun_n_r_1').src = 'img/s.svg';
        sun_n_r_1 = false;
    } 
    else{
        document.getElementById('sun_n_r_1').src = 'img/s2.svg';
        sun_n_r_1 = true;
    }   
}
function power_n_r_1_img() {
    if (power_n_r_1) {
        document.getElementById('power_n_r_1').src = 'img/o.svg';
        power_n_r_1 = false;
    } 
    else{
        document.getElementById('power_n_r_1').src = 'img/o2.svg';
        power_n_r_1 = true;
    }   
}

function n_r_1_vista(x) {
    n_r_1_luz = document.getElementById('n_r_1_luz');
    n_r_1_apagado = document.getElementById('n_r_1_apagado');

    switch(x){
        
        case 1:
        n_r_1_luz.style.display = '';
        n_r_1_apagado.style.display = 'none';
        break;

        case 0:
        n_r_1_luz.style.display = 'none';
        n_r_1_apagado.style.display = '';
        break;

        default:
        n_r_1_luz.style.display = 'none';
        n_r_1_apagado.style.display = 'none';
    }
}
var rain_n_r_2 = true;
var power_n_r_2 = true;

function n_r_2_img (selector)
{
    switch(selector) 
    {
    case 1:
        rain_n_r_2 = false;
        power_n_r_2 = true; 
        fresh_2();
        break;
    case 0:
        rain_n_r_2 = true;
        power_n_r_2 = false; 
        fresh_2();
        break;
    default:
        fresh_2();
    }
}

function fresh_2()
{
    rain_n_r_2_img();
    power_n_r_2_img();
}

function rain_n_r_2_img() {
    if (rain_n_r_2) {
        document.getElementById('rain_n_r_2').src = 'img/r.svg';
        rain_n_r_2 = false;
    } 
    else{
        document.getElementById('rain_n_r_2').src = 'img/r2.svg';
        rain_n_r_2 = true;
    }   
}
function power_n_r_2_img() {
    if (power_n_r_2) {
        document.getElementById('power_n_r_2').src = 'img/o.svg';
        power_n_r_2 = false;
    } 
    else{
        document.getElementById('power_n_r_2').src = 'img/o2.svg';
        power_n_r_2 = true;
    }   
}

function n_r_2_vista(x) {
    n_r_2_lluvia = document.getElementById('n_r_2_lluvia');
    n_r_2_apagado = document.getElementById('n_r_2_apagado');
    switch(x){

        case 1:
        n_r_2_lluvia.style.display = '';
        n_r_2_apagado.style.display = 'none';
        break;

        case 0:
        n_r_2_lluvia.style.display = 'none';
        n_r_2_apagado.style.display = '';
        break;

        default:
        n_r_2_lluvia.style.display = 'none';
        n_r_2_apagado.style.display = 'none';
    }
}
var temp_n_r_3 = true;
var power_n_r_3 = true;

function n_r_3_img (selector)
{
    switch(selector) 
    {
    case 1:
        temp_n_r_3 = false;
        power_n_r_3 = true; 
        fresh_3();
        break;
    case 0:
        temp_n_r_3 = true;
        power_n_r_3 = false; 
        fresh_3();
        break;
    default:
        fresh_3();
    }
}

function fresh_3()
{
    temp_n_r_3_img();
    power_n_r_3_img();
}

function temp_n_r_3_img() {
    if (temp_n_r_3) {
        document.getElementById('temp_n_r_3').src = 'img/t.svg';
        temp_n_r_3 = false;
    } 
    else{
        document.getElementById('temp_n_r_3').src = 'img/t2.svg';
        temp_n_r_3 = true;
    }   
}
function power_n_r_3_img() {
    if (power_n_r_3) {
        document.getElementById('power_n_r_3').src = 'img/o.svg';
        power_n_r_3 = false;
    } 
    else{
        document.getElementById('power_n_r_3').src = 'img/o2.svg';
        power_n_r_3 = true;
    }   
}

function n_r_3_vista(x) {
    n_r_3_temp = document.getElementById('n_r_3_temp');
    n_r_3_apagado = document.getElementById('n_r_3_apagado');

    switch(x){
        
        case 1:
        n_r_3_temp.style.display = '';
        n_r_3_apagado.style.display = 'none';
        break;

        case 0:
        n_r_3_temp.style.display = 'none';
        n_r_3_apagado.style.display = '';
        break;

        default:
        n_r_3_temp.style.display = 'none';
        n_r_3_apagado.style.display = 'none';
    }
}
var salida_n_r_4 = true;
var power_n_r_4 = true;

function n_r_4_img (selector)
{
    switch(selector) 
    {
    case 1:
        salida_n_r_4 = false;
        power_n_r_4 = true; 
        fresh_4();
        break;
    case 0:
        salida_n_r_4 = true;
        power_n_r_4 = false; 
        fresh_4();
        break;
    default:
        fresh_4();
    }
}

function fresh_4()
{
    salida_n_r_4_img();
    power_n_r_4_img();
}

function salida_n_r_4_img() {
    if (salida_n_r_4) {
        document.getElementById('salida_n_r_4').src = 'img/p.svg';
        salida_n_r_4 = false;
    } 
    else{
        document.getElementById('salida_n_r_4').src = 'img/p2.svg';
        salida_n_r_4 = true;
    } 
}
function power_n_r_4_img() {
    if (power_n_r_4) {
        document.getElementById('power_n_r_4').src = 'img/o.svg';
        power_n_r_4 = false;
    } 
    else{
        document.getElementById('power_n_r_4').src = 'img/o2.svg';
        power_n_r_4 = true;
    }   
}

function n_r_4_vista(x) {
    n_r_4_salida = document.getElementById('n_r_4_salida');
    n_r_4_apagado = document.getElementById('n_r_4_apagado');

    switch(x){
        
        case 1:
        n_r_4_salida.style.display = '';
        n_r_4_apagado.style.display = 'none';
        modo[3] = 1;
        break;

        case 0:
        n_r_4_salida.style.display = 'none';
        n_r_4_apagado.style.display = '';
        modo[3] = 0;
        break;

        default:
        n_r_4_salida.style.display = 'none';
        n_r_4_apagado.style.display = 'none';
    }
}