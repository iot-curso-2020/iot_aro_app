    
 // <script type='text/javascript' src='https://www.gstatic.com/charts/loader.js'></script>    google.charts.load('current', {'packages':['corechart']});
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(gtemp);
    google.charts.setOnLoadCallback(ghumedad);
    var t1 = [19,18,17,18,19,20,21,25,27,28,27,25,21,20,19,18,17,16,12,12,7,6,10,13];
    var hora = ['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','00'];
    var t2 = [60,58,55,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25];
    var request;
function graficos() 
{
     
      request = new XMLHttpRequest(); 
      request.onreadystatechange = function() 
      {
        if (this.readyState == 4) {
          if (this.status == 200) { 
            if (this.responseXML != null) {
              
              var count;
              var num = this.responseXML.getElementsByTagName('T1').length;  
              for (count = 0; count < num; count++) 
              {
              t1[count] = parseInt(this.responseXML.getElementsByTagName('T1')[count].childNodes[0].nodeValue);
              t2[count] = parseInt(this.responseXML.getElementsByTagName('T2')[count].childNodes[0].nodeValue);
              hora[count] = this.responseXML.getElementsByTagName('T3')[count].childNodes[0].nodeValue;
              }
            }
          }
        }
      }
      request.open('GET', 'http://'+ ip + ':' + p + '/'  + 'g.xml' , true);
      request.send(null);

     setTimeout('ghumedad()', 50);
     setTimeout('gtemp()', 50);
     setTimeout('ghumedad()', 500);
     setTimeout('gtemp()', 500); 
}

function gtemp()
    {
      var data = google.visualization.arrayToDataTable([
            ['HORA', ''],
            [hora[0]+'H',  t1[0]],
            [hora[1]+'H',  t1[1]],
            [hora[2]+'H',  t1[2]],
            [hora[3]+'H',  t1[3]],
            [hora[4]+'H',  t1[4]],
            [hora[5]+'H',  t1[5]],
            [hora[6]+'H',  t1[6]],
            [hora[7]+'H',  t1[7]],
            [hora[8]+'H',  t1[8]],
            [hora[9]+'H',  t1[9]],
            [hora[10]+'H',  t1[10]],
            [hora[11]+'H',  t1[11]],
            [hora[12]+'H',  t1[12]],
            [hora[13]+'H',  t1[13]],
            [hora[14]+'H',  t1[14]],
            [hora[15]+'H',  t1[15]],
            [hora[16]+'H',  t1[16]],
            [hora[17]+'H',  t1[17]],
            [hora[18]+'H',  t1[18]],
            [hora[19]+'H',  t1[19]],
            [hora[20]+'H',  t1[20]],
            [hora[21]+'H',  t1[21]],
            [hora[22]+'H',  t1[22]],
            [hora[23]+'H',  t1[23]]
          ]);

          var options = {
            'legend':'none',
            'title' : '0',
            'titleTextStyle' :  { 
                              color :  '#D24D57' , 
                              fontName :  'Verdana' , 
                              fontSize :  40 
                            },
            'series': {  0: { color: '#D24D57' }  },
            'lineWidth': 5,
            'pieSliceText': 'value',
            'backgroundColor': '#353535',
            'marginwidth': 0,
            'is3D': true,
            hAxis: {
            textStyle: {
              color: '#ffffff',
              fontSize: 12,
              fontName: 'Arial',
              bold: false,
              italic: true
            },
            titleTextStyle: {
              color: '#01579b',
              fontSize: 1,
              fontName: 'Arial',
              bold: false,
              italic: true
            }
          },
        vAxis: {
          textStyle: {
            color: '#ffffff',
            fontSize: 12,
            bold: false
          },
          titleTextStyle: {
            color: '#ffffff',
            fontSize: 1,
            bold: true
          }
        },
    };

    var chart = new google.visualization.LineChart(document.getElementById('gtemp'));
          chart.draw(data, options);
/*    setTimeout('gtemp()', 1000); */
}

function ghumedad()
    {
      var data = google.visualization.arrayToDataTable([
            ['HORA', ''],
            [hora[0]+'H',  t2[0]],
            [hora[1]+'H',  t2[1]],
            [hora[2]+'H',  t2[2]],
            [hora[3]+'H',  t2[3]],
            [hora[4]+'H',  t2[4]],
            [hora[5]+'H',  t2[5]],
            [hora[6]+'H',  t2[6]],
            [hora[7]+'H',  t2[7]],
            [hora[8]+'H',  t2[8]],
            [hora[9]+'H',  t2[9]],
            [hora[10]+'H',  t2[10]],
            [hora[11]+'H',  t2[11]],
            [hora[12]+'H',  t2[12]],
            [hora[13]+'H',  t2[13]],
            [hora[14]+'H',  t2[14]],
            [hora[15]+'H',  t2[15]],
            [hora[16]+'H',  t2[16]],
            [hora[17]+'H',  t2[17]],
            [hora[18]+'H',  t2[18]],
            [hora[19]+'H',  t2[19]],
            [hora[20]+'H',  t2[20]],
            [hora[21]+'H',  t2[21]],
            [hora[22]+'H',  t2[22]],
            [hora[23]+'H',  t2[23]]
          ]);

          var options = {
            'legend':'none',
            'title' : '2',
            'titleTextStyle' :  { 
                              color :  '#62a8ea' , 
                              fontName :  'Verdana' , 
                              fontSize :  40 
                            },
            'series': {  0: { color: '#62a8ea' }  },
            'lineWidth': 5,
            'pieSliceText': 'value',
            'backgroundColor': '#353535',
            'marginwidth': 0,
            'is3D': true,
            hAxis: {
            textStyle: {
              color: '#ffffff',
              fontSize: 12,
              fontName: 'Arial',
              bold: false,
              italic: true
            },
            titleTextStyle: {
              color: '#62a8ea',
              fontSize: 13,
              fontName: 'Arial',
              bold: false,
              italic: true
            }
          },
        vAxis: {
          textStyle: {
            color: '#ffffff',
            fontSize: 12,
            bold: false
          },
          titleTextStyle: {
            color: '#62a8ea',
            fontSize: 13,
            bold: true
          }
        }
    }

var chart = new google.visualization.LineChart(document.getElementById('ghumedad'));
chart.draw(data, options);
/*setTimeout('ghumedad()', 1000); */
}
    //var repeticion = window.setInterval('temperatura()',1000);
    //onclick='window.clearInterval(repeticion);' ontouchcancel='window.clearInterval(repeticion);'

    //$(window).resize(function(){ gtemp();  ghumedad(); });
