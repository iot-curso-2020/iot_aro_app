var sun_n_r_1 = true;
var power_n_r_1 = true;

function n_r_1_img (selector)
{
    switch(selector) 
    {
    case 1:
        sun_n_r_1 = false;
        power_n_r_1 = true; 
        fresh_1();
        break;
    case 0:
        sun_n_r_1 = true;
        power_n_r_1 = false; 
        fresh_1();
        break;
    default:
        fresh_1();
    }
}

function fresh_1()
{
    sun_n_r_1_img();
    power_n_r_1_img();
}

function sun_n_r_1_img() {
    if (sun_n_r_1) {
        document.getElementById('sun_n_r_1').src = 'img/s.svg';
        sun_n_r_1 = false;
    } 
    else{
        document.getElementById('sun_n_r_1').src = 'img/s2.svg';
        sun_n_r_1 = true;
    }   
}
function power_n_r_1_img() {
    if (power_n_r_1) {
        document.getElementById('power_n_r_1').src = 'img/o.svg';
        power_n_r_1 = false;
    } 
    else{
        document.getElementById('power_n_r_1').src = 'img/o2.svg';
        power_n_r_1 = true;
    }   
}

function n_r_1_vista(x) {
    n_r_1_luz = document.getElementById('n_r_1_luz');
    n_r_1_apagado = document.getElementById('n_r_1_apagado');

    switch(x){
        
        case 1:
        n_r_1_luz.style.display = '';
        n_r_1_apagado.style.display = 'none';
        modo[1] = '1';
        break;

        case 0:
        n_r_1_luz.style.display = 'none';
        n_r_1_apagado.style.display = '';
        modo[1] = '0';
        break;

        default:
        n_r_1_luz.style.display = 'none';
        n_r_1_apagado.style.display = 'none';
    }
}
