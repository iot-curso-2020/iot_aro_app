var temp_n_r_3 = true;
var power_n_r_3 = true;

function n_r_3_img (selector)
{
    switch(selector) 
    {
    case 1:
        temp_n_r_3 = false;
        power_n_r_3 = true; 
        fresh_3();
        break;
    case 0:
        temp_n_r_3 = true;
        power_n_r_3 = false; 
        fresh_3();
        break;
    default:
        fresh_3();
    }
}

function fresh_3()
{
    temp_n_r_3_img();
    power_n_r_3_img();
}

function temp_n_r_3_img() {
    if (temp_n_r_3) {
        document.getElementById('temp_n_r_3').src = 'img/tr3.svg';
        temp_n_r_3 = false;
    } 
    else{
        document.getElementById('temp_n_r_3').src = 'img/t2.svg';
        temp_n_r_3 = true;
    }   
}
function power_n_r_3_img() {
    if (power_n_r_3) {
        document.getElementById('power_n_r_3').src = 'img/o.svg';
        power_n_r_3 = false;
    } 
    else{
        document.getElementById('power_n_r_3').src = 'img/o2.svg';
        power_n_r_3 = true;
    }   
}

function n_r_3_vista(x) {
    n_r_3_temp = document.getElementById('n_r_3_temp');
    n_r_3_apagado = document.getElementById('n_r_3_apagado');

    switch(x){
        
        case 1:
        n_r_3_temp.style.display = '';
        n_r_3_apagado.style.display = 'none';
        modo[3] = '1';
        break;

        case 0:
        n_r_3_temp.style.display = 'none';
        n_r_3_apagado.style.display = '';
        modo[3] = '0';
        break;

        default:
        n_r_3_temp.style.display = 'none';
        n_r_3_apagado.style.display = 'none';
    }
}
