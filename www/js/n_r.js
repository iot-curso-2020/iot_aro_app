function n_r(x) {
    r1 = document.getElementById('r1');
    r2 = document.getElementById('r2');
    r3 = document.getElementById('r3');
    r4 = document.getElementById('r4');
    switch(x){
        case 1:
        r1.style.display = '';
        r2.style.display = 'none';
        r3.style.display = 'none';
        r4.style.display = 'none';
        break;

        case 2:
        r1.style.display = 'none';
        r2.style.display = '';
        r3.style.display = 'none';
        r4.style.display = 'none';    
        break;

        case 3:
        r1.style.display = 'none';
        r2.style.display = 'none';
        r3.style.display = '';
        r4.style.display = 'none';
        break;

        case 4:
        r1.style.display = 'none';
        r2.style.display = 'none';
        r3.style.display = 'none';
        r4.style.display = '';
        break;        

        default:
        r1.style.display = '';
        r2.style.display = 'none';
        r3.style.display = 'none';
        r4.style.display = 'none';
    }
}

