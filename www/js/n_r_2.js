var rain_n_r_2 = true;
var power_n_r_2 = true;

function n_r_2_img (selector)
{
    switch(selector) 
    {
    case 1:
        rain_n_r_2 = false;
        power_n_r_2 = true; 
        fresh_2();
        break;
    case 0:
        rain_n_r_2 = true;
        power_n_r_2 = false; 
        fresh_2();
        break;
    default:
        fresh_2();
    }
}

function fresh_2()
{
    rain_n_r_2_img();
    power_n_r_2_img();
}

function rain_n_r_2_img() {
    if (rain_n_r_2) {
        document.getElementById('rain_n_r_2').src = 'img/r.svg';
        rain_n_r_2 = false;
    } 
    else{
        document.getElementById('rain_n_r_2').src = 'img/r2.svg';
        rain_n_r_2 = true;
    }   
}
function power_n_r_2_img() {
    if (power_n_r_2) {
        document.getElementById('power_n_r_2').src = 'img/o.svg';
        power_n_r_2 = false;
    } 
    else{
        document.getElementById('power_n_r_2').src = 'img/o2.svg';
        power_n_r_2 = true;
    }   
}

function n_r_2_vista(x) {
    n_r_2_lluvia = document.getElementById('n_r_2_lluvia');
    n_r_2_apagado = document.getElementById('n_r_2_apagado');
    switch(x){

        case 1:
        n_r_2_lluvia.style.display = '';
        n_r_2_apagado.style.display = 'none';
        modo[2] = '1';
        break;

        case 0:
        n_r_2_lluvia.style.display = 'none';
        n_r_2_apagado.style.display = '';
        modo[2] = '0';
        break;

        default:
        n_r_2_lluvia.style.display = 'none';
        n_r_2_apagado.style.display = 'none';
    }
}
